﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace Patent_Analyzer.Forms
{
    public partial class FormResult : Form
    {
        public FormResult()
        {
            InitializeComponent();
            tbSimilar.Text =
                "RU142153U1 2014.06.20 \nRU2681361C1 2019.03.06 \nRU2656841C2 2018.06.06 \nRU2444055C1 2012.02.27 \nRU2242047C1 2004.12.10 \nRU2386166C2 2010.04.10 \nRU98832U1 2010.10.27 \nRU2612326C2 2017.03.07 \nRU2702269C1 2019.10.07 \nRU2747476C1 2021.05.05 \nRU134679U1 2013.11.20 \nRU113384U1 2012.02.10 \nRU35452U1 2004.01.10 \nRU2571784C1 2015.12.20\nRU2642365C1 2018.01.24 \nRU2727558C1 2020.07.22 ";
            tbPatentCat.Text = "G06F 16/29\nG06T 17/05";

            tbText.AppendText("Область техники, к которой относится полезная модель Полезная модель относится к ");
            tbText.AppendText("вычислительной технике ", Color.Crimson);
            tbText.AppendText("и системам моделирования и может быть использована для обработки информации, а именно ");
            tbText.AppendText("геопространственных ", Color.Crimson);
            tbText.AppendText(" данных различных типов для создания многоуровневых цифровых интеллектуальных  ");
            tbText.AppendText("геоинформационных ", Color.Crimson);
            tbText.AppendText(" сред и ");
            tbText.AppendText("картографических", Color.Crimson);
            tbText.AppendText(" моделей местности и/или решения прикладных задач с использованием геопространственных данных.");
        }


       
    }
    
    
    

    public static class RichTextBoxExtensions
    {
        public static void AppendText(this RichTextBox box, string text, Color color)
        {
            box.SelectionStart = box.TextLength;
            box.SelectionLength = 0;

            box.SelectionColor = color;
            box.AppendText(text);
            box.SelectionColor = box.ForeColor;
        }
    }
}