﻿using System;
using System.Windows.Forms;

namespace Patent_Analyzer.Forms
{
    public partial class FormEnterText : Form
    {
        private Form activeForm;
        private Form1 main;
        public FormEnterText(Form1 form)
        {
            InitializeComponent();
            main = form;
        }

     



        private void button1_Click(object sender, EventArgs e)
        {
            Form childForm = new FormResult();
            if (activeForm != null)
            {
                activeForm.Close();
            }

            main.activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            main.Controls.Add(childForm);
            main.PanelDesktop.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
        }
    }
}