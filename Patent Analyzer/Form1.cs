﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Patent_Analyzer.Forms;

namespace Patent_Analyzer
{
    public partial class Form1 : Form
    {

        public Form activeForm;
        public Panel PanelDesktop;
        
        public Form1()
        {
            InitializeComponent();
            PanelDesktop = this.panelDesktop;
        }

        public void OpenChildForm(Form childForm, object btnSender)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }

            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            this.panelDesktop.Controls.Add(childForm);
            this.panelDesktop.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
            switch (childForm.Text)
            {
                case "FormEnterText":
                case "FormResult":
                    labelTitle.Text = "Анализ текста патента";
                    break;
                case "FormHelp":
                    labelTitle.Text = "Справка";
                    break;
                case "FormHistory":
                    labelTitle.Text = "История запросов";
                    break;
            }
            
        }

        private void buttonAnalyze_Click(object sender, EventArgs e)
        {
            OpenChildForm(new FormEnterText(this), sender);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenChildForm(new FormHistory(), sender);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            OpenChildForm(new FormHelp(), sender);
        }
    }
    
}
